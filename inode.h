#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <stdbool.h>

typedef struct inode{
	inode* next;
	int N;
	bool readable;
	bool writable;
	bool executable;
	unsigned int uid; //the owner of this file
	unsigned int size; //size of the file
	unsigned int time; // last acceess
	unsigned int ctime; //time was this files wsas created
	unsigned int mtime; //time when this file was modified 
	unsigned int dtime; //time was this files wsas delted 
	unsigned int blocks[N]; //set of blocks 
};

struct inode* inodebuilder(unsigned int uid_,unsigned int time_){
	struct inode* new_inode = (struct inode*) malloc(sizeof(struct inode));
    readable=writable=executable=1;
    uid = uid_;
    /*https://rcruzn.wordpress.com/2013/04/24/dame-nombre-de-usuario-en-c-linux/*/
    time = time_;
    blocks[N] = NULL;
    return inode;
}

bool change_time(struct inode* inode,unsigned int time_){
	inode->time = time_;
	return 1;
}

bool change_ctime(struct inode* inode,unsigned int time_){
	inode->ctime = time_;
	return 1;
}


bool change_mtime(struct inode* inode,unsigned int time_){
	inode->mtime = time_;
	return 1;
}

bool change_dtime(struct inode* inode,unsigned int time_){
	inode->dtime = time_;
	return 1;
}