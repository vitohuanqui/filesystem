#ifndef LISTEC_H_INCLUDE
#define LISTEC_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <stdbool.h>

typedef struct SuperBlock
{
	char fs_name[15];
	//for blocks
	unsigned int superblock_count; //number of blocks SB can be saved 
	unsigned int block_size; // size of block
	unsigned int block_count; // number of blocks saved
    unsigned int dbitmap_count; //number of blocks block databitmap occupies
    unsigned int free_blocks; //number iof blocks are free
    unsigned int fs_reserved_blocks; //number of blocks are reserved from file system structs
    //for inodes
    unsigned int inode_size; //number of block an inode indexes
    unsigned int inode_count; //number of blocks occupied by inodes
    unsigned int ibitmap_count; //number of blocks occupied by inodebitmap 
    unsigned int fs_reserved_inodes; //number of inodes reserved from file system structures
 
    unsigned int dir_table_count; //number of blocks the directory table occupies
};

enum type{
	type_int=0,
	type_bool=1,
	type_char=2,
	type_float=3,
	type_double=4,
	type_string=5
};

typedef struct Lnode{
	void* L_data;
	struct Lnode* _next;
	enum type type;
};

typedef struct list{
	struct Lnode* root;
	struct Lnode** last;
};

struct list* builder()
{
    struct list* build_list=(struct list*) malloc(sizeof(struct list));
    build_list->root=NULL;
    build_list->last=&(build_list->root);
    return build_list;
};

bool find1(struct list* list_, void* data, enum type type_,struct Lnode* aux_list)
{
	aux_list= (list_->root);
	if (aux_list->L_data == data){	
		return 1;
	}
    while(aux_list!=NULL)
    {
        if (aux_list->_next->L_data == data){
        	return 1;
        }
        aux_list = aux_list->_next;
    }
    return 0;
};

bool find(struct list* list_, void* data, enum type type_)
{
	struct Lnode* aux;
	return find1(list_,data,type_,aux);
}

bool push_element(struct list* list_, void* data,enum type type_)
{
	if (find(list_,data,type_)){
		return 0;
	}
    struct Lnode** new_node=list_->last;
    (*new_node) = (struct Lnode*) malloc(sizeof(struct Lnode));
    (*new_node)->L_data = data;
    (*new_node)->type=type_;
    (*new_node)->_next=NULL;
    list_->last=&((*new_node)->_next);
    return 1;
}


/*bool remove(struct list* list_, void* data,enum type type_){
	Lnode* aux,tmp;
	if(find1(list_,data,aux,type)){
		tmp = aux->_next->_next;
		delete aux->_next;
		aux->_next=tmp;
		return 1;
	}
	return 0;
}*/
void print_list(struct list* aux_h)
{
    struct Lnode* aux_list= aux_h->root;
    while(aux_list!=NULL)
    {
        switch(aux_list->type)
        {
            case type_int:
                printf("%d -> ",aux_list->L_data);
                break;
            case type_char:
                printf("%c -> ",aux_list->L_data);
                break;
            case type_float:
                printf("%f -> ",aux_list->L_data);
                break;
            case type_double:
                printf("%d -> ",aux_list->L_data);
                break;
            case type_string:
                printf("%s -> ",aux_list->L_data);
                break;
            default:
                printf("Dont type -> ");
        }
        aux_list = aux_list->_next;
    }
}
#endif // LISTEC_H_INCLUDED
