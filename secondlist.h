#include "inode.h"

typedef struct Listinode{
	struct inode* root;
	struct inode** last;
    int N;
};

struct Listinode* secondlistbuilder(int n)
{
    struct Listinode* build_list=(struct Listinode*) malloc(sizeof(struct Listinode));
    build_list->root=NULL;
    build_list->last=&(build_list->root);
    build_list->N = n;
    return build_list;
};

bool save(struct Listinode* list, void* data){
    struct inode** new_node=list->last;
    (*new_node) = (struct inode*) malloc(sizeof(struct inode));
    int size = (size(data)/(32768))+1;
    int c = 0;
    while (c!=size){
        (*new_node)->blocks[c] = data[:32768];
        data = data[32769:];
        c++;
    }
    (*new_node)->data = data;
    (*new_node)->next=NULL;
    list->last=&((*new_node)->next);
    return 1;
}

bool insert(struct Listinode* list, void* data)
{
    int size = (size(data)/(32768*list->N))+1;
    int c = 0;
    while (c!=size){
        save(list,data[:32768*list->N])
        data = data[32769*list->N:];
        c++;
    }
    return 1;
}


bool deletefile(struct Listnode* list){
	struct inode** aux,tmp;
	aux = &(list->root);
	while (aux!=NULL){
		tmp = &aux->next;
		delete aux;
		tmp=aux;
	}
	return 1;
}
void print_list(struct list* aux_h)
{
    struct Lnode* aux_list= aux_h->root;
    while(aux_list!=NULL)
    {
        switch(aux_list->type)
        {
            case type_int:
                printf("%d -> ",aux_list->L_data);
                break;
            case type_char:
                printf("%c -> ",aux_list->L_data);
                break;
            case type_float:
                printf("%f -> ",aux_list->L_data);
                break;
            case type_double:
                printf("%d -> ",aux_list->L_data);
                break;
            case type_string:
                printf("%s -> ",aux_list->L_data);
                break;
            default:
                printf("Dont type -> ");
        }
        aux_list = aux_list->_next;
    }
}