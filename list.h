#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <stdbool.h>

typedef struct Node{
	int data;
	struct Node* next;
};

typedef struct List{
	struct Node* root;
	struct Node** last;
};

struct List* builder()
{
    struct List* build_list=(struct List*) malloc(sizeof(struct List));
    build_list->root=NULL;
    build_list->last=&(build_list->root);
    return build_list;
};

bool find1(struct List* list, int data, struct Node* aux_node)
{
    while(aux_list!=NULL)
    {
        printf("%d -> ",aux_list->data);
        aux_list = aux_list->next;
    }

	*aux_node = (list->root);
    if ((*aux_node)==NULL){
        return 0;
    }
	if ((*aux_node)->data == data){	
		return 1;
	}
    while((*aux_node)->next!=NULL)
    {
        if ((*aux_node)->next->data == data){
        	return 1;
        }
        (*aux_node) = (*aux_node)->next;
    }
    return 0;
};

bool find(struct List* list, int data)
{
	struct Node* aux;
	return find1(list,data,aux);
}

bool push_element(struct List* list, int data)
{
	if (find(list,data)){
		return 0;
	}
    struct Node** new_node=list->last;
    (*new_node) = (struct Node*) malloc(sizeof(struct Node));
    (*new_node)->data = data;
    (*new_node)->next=NULL;
    list->last=&((*new_node)->next);
    return 1;
}


/*bool remove(struct list* list_, void* data,enum type type_){
	Lnode* aux,tmp;
	if(find1(list_,data,aux,type)){
		tmp = aux->_next->_next;
		delete aux->_next;
		aux->_next=tmp;
		return 1;
	}
	return 0;
}*/
void print_list(struct List* aux_h)
{
    struct Node* aux_list= aux_h->root;
    while(aux_list!=NULL)
    {
        printf("%d -> ",aux_list->data);
        aux_list = aux_list->next;
    }
}